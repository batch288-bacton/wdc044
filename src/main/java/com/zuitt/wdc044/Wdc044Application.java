package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" is an example of "Annotations" mark.
	// Annotations are used to provide supplemental information about the program.
	// These are used to manage and configure the behavior of the framework.
	// Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
	// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

//This specifies the main class of the springboot application.

@SpringBootApplication

//This will indecates that a class is a controller that will handle RESTful web request and returns an HTTP response.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		//This method starts the whole Spring Framework.
		//This serves as the entry point to start the application.
		SpringApplication.run(Wdc044Application.class, args);
	}
	//This is used to map HTTP GET requests.
		//Note: HTTP Request annotations is usually followed by a "method body"
		//The "method body" contains the logic to generate the response.
	@GetMapping("/hello")
	//"@RequestParam" is used to extract query parameters, form parameters, and even files from the request.
		//if the URL is: /hello?name=john the method will return "Hello john"
			// "?" means that the start of the parameters followed bu the "key=value" pair.
		//if the URL is: /hello, the method will return "Hello World."
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		//To send a response of "Hello + name"
		// "%s!" is a placeholder for the variable name
		return String.format("Hello %s!", name);
	}

// Activity s01
	//localhost:8080/hi
	//localhost:8080/hi?user=Sadam
	@GetMapping("/hi")

	public String hi(@RequestParam(value = "user", defaultValue = "user") String name){
		return  String.format("Hi %s!", name);
	}

//  Activity Stretch Goal:
	// localhost:8080/nameAge
	@GetMapping("/nameAge")

	public String nameAge(@RequestParam(value = "name", defaultValue = "Guest") String name, @RequestParam(value = "age", defaultValue = "18") Integer age) {
		if (name == null || name.isEmpty() || age == null) {
			return "Please provide name and age";
		}
		return String.format("Hello %s you are %d years old.", name, age);
	}

}
